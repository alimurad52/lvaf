	
<?php /* Template Name: Projects */ 
get_header();?>
<div class="lyh-projects">
	<div class="container">
		<div class="page-navigation row">
			<h1 class="col-md-8">Our latest projects</h1>
			<div class="form-group industry-dropdown col-md-2">
				<label for="industry-dropdown">Industry</label>
				<select class="form-control" id="industry-dropdown">
					<option>All</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
			<div class="form-group media-dropdown col-md-2">
				<label for="media-dropdown">Media</label>
				<select class="form-control" id="media-dropdown">
					<option>Screendesign</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
		</div>
	</div>
	<div class="container-fluid all-projects">
		<?php
			$args = array(
				'numberposts' => 8,
				'orderby' => 'post_date',
				'order' => 'ASC',
				'post_type' => 'post'
			);
			$our_projects = get_posts($args);
			$x = 0;
			foreach($our_projects as $our_project) {
				echo "<div class='col-md-4' style='padding: 0;'>";
				echo get_the_post_thumbnail($our_project->ID);
				echo "<div class='after'></div>";
				$category = get_the_category($our_project->ID);
				$category_parent_id = $category[0]->category_parent;
				$category_parent = get_term($category_parent_id);
				$css_slug = $category_parent->slug." / ".$category[0]->slug;
				echo "<label class='lbl_category'>".$css_slug."</label>";
				echo "<h1 class='post_title'>".$our_project->post_title."</h1>";
				echo "<a href='".get_permalink($our_project->ID)."'><input type='button' class='btn-project' value='Discover this project' ></a>";
				echo "</div>";
			}
			?>
	</div>
	<?php include 'contact.php' ?>
	<?php get_footer(); ?>
</div>