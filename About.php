	
<?php /* Template Name: About */ 
get_header();?>
<div class="about-us">
	<div class="jumbotron about-page" style="background: linear-gradient(to right, rgba(236, 25, 68, 0.7), rgba(132, 45, 106, 1)), url('<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/about_bg.png');background-size: cover">
		<p>We're a premium digital agency that specialises in developing and implementing digital eCommerce and branding strategies.</p>
	</div>
	<div class="our-services row">
		<h2 class="col-md-3 title_our_services">Our Services</h2>
		<div class="col-md-3">
			<h2>Stategy</h2>
			<p>Digital Strategy</p>
			<p>eCommerce Strategy</p>
			<p>Digital consulting</p>
			<p>Strategy & Research</p>
			<p>Brand Architecture</p>
		</div>
		<div class="col-md-3">
			<h2>Development</h2>
			<p>Responsive Development</p>
			<p>Custom Web Applications</p>
			<p>eCommerce</p>
			<p>System Integration</p>
			<p>Hosting</p>
		</div>
		<div class="col-md-3">
			<h2>Design</h2>
			<p>User Experience Design</p>
			<p>User Interface Design</p>
			<p>Responsive Design</p>
			<p>Logo Design</p>
			<p>Brand Guidelines</p>
			<p>Collateral</p>
			<p>Re-branding</p>
		</div>
	</div>
	<div class="">
		<div class="our-goals container-fluid">
			<img src="<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/about-1.png" />
			<div class='after'></div>
			<h2>Our Goals</h2>
			<p>We help our clients understand what they can achieve with the right strategy and branding.</p>
		</div>
		<div class="our-family container-fluid">
			<img src="<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/about-2.png" />
			<div class='after'></div>
			<h2>Our Family</h2>
			<p>We start by providing a tailored team, specific to our clients needs, analysing teh best industry, brand and technically viable options to delivery teh best possbile outcome.</p>
		</div>
		<div class="our-experience container-fluid">
			<img src="<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/about-3.png" />
			<div class='after'></div>
			<h2>Our Experience</h2>
			<p>We draw on more than 40 years of combined industry experience to provide our clients with the right tools to achieve their digital marketing goals.</p>
		</div>
		<div class="our-approach row">
			<h2 class="col-md-4">Our Approach</h2>
			<span class="col-md-4"><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p><br /><br /><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p></span>
			<p class="col-md-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
		</div>
		<div class="our-clients">
			<div class="container-fluid">
				<div class="col-md-3">
					<h2>Our Clients</h2>
					<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
					<input type="button" value="See what we've done" class="btn-lva-main-inverse">
				</div>
				<?php echo do_shortcode("[smls id='95']"); ?>
			</div>
		</div>
	</div>
	<?php include 'contact.php' ?>
	<?php get_footer(); ?>
</div>

