module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			options: {
				sourceMap: true
			},
			dist: {
				files: [
					{'dist/css/style.css': 'src/scss/base.scss'}
				]
			}
		},
		concat: {
			js: {
				files: {
					'dist/js/scripts.js': ['src/js/*.js'],
				}
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			dist: {
				files: [ 
					{'dist/js/scripts.min.js': ['dist/js/scripts.js']}
				]
			}
		},
		jshint: {
			files: ['dist/js/scripts.min.js'],
			options: {
			// options here to override JSHint defaults
				globals: {
					jQuery: true,
					console: true,
					module: true,
					document: true
				}
			}
		},
		lineending: {               // Task
				dist: {                   // Target
					options: {              // Target options
						eol: 'crlf',
						overwrite: true
					},
					files: {                // Files to process
						'': ['dist/js/scripts.min.js', 'dist/css/style.min.css' ]
					}
				}
		},
		cssmin: {
			options: {
				mergeIntoShorthands: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'dist/css/style.min.css': ['dist/css/style.css'],
				}
			}
		},
		watch: {
			scripts: {
				files: ['src/js/*.js', 'assets/admin/src/js/*.js'],
				tasks: ['concat', 'uglify'],
				options: {
					interrupt: true,
				},
			},
			css : {
				files: ['src/scss/*.scss', 'src/scss/*/*.scss'],
				tasks: ['sass', 'cssmin', 'lineending'],
				options: {
					interrupt: true,
				},
			},
		},
	});
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-lineending');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default', ['sass', 'cssmin', 'uglify', 'concat', 'lineending']);
}