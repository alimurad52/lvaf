	
<?php /* Template Name: WriteUs */ 
get_header();?>
<div class="write-us">
	<div class="page-navigation">
		<div class="container">
			<h3>Contact</h3>
			<p>Your digital strategy is just a phone call away!<br />Let us know how we can help you get ahead.</p>
			<div class="row btn-group">
				<a class="col-sm-3" style="padding-left: 0;"><span><input type="button" class="social-media_btn" value="facebook.com/lyrehouse" ></span></a>
				<a class="col-sm-3"><span><input type="button" class="social-media_btn" value="gplus.com/lyrehouse" ></span></a>
				<a class="col-sm-3"><span><input type="button" class="social-media_btn" value="instagram.com/lyrehouse" ></span></a>
				<a class="col-sm-3"><span><input type="button" class="social-media_btn" value="twitter.com/lyrehouse" ></span></a>
			</div>
		</div>
	</div>
	<div class="map">
		<div style="max-width:100%;overflow:hidden;color:red;height:400px;" class="google_map">
			<div id="embeddedmap-display" style="height:100%; width:100%;max-width:100%;">
				<iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Aachen,+Germany&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8">
				</iframe>
			</div>
		</div>
		<div class="map_address container">
			<div class="row">
				<div class="address">
					<h3>Location <strong>Aachen</strong></h3>
					<div class="col col-lg-6">
						<p>Wurmberenden 17 <br />52070 Aachen<br />Germany</p>
					</div>
					<div class="col col-lg-5">
						<p>Tel. +49 123.456.789 <br />aachen@lyrehouse.de</p>
					</div>
					<input type="Button" class="btn-lva-main" value="Find us on Google Maps">
				</div>
				<div class="desc">
					<p>The Bilby is a premiun, digital agency that specialises in developing realistic, results-driven digital and eCommerce strategies for small to mid-size companies. Let us know how we can help, we'd love to hear from you!</p>
				</div>
			</div>
			<div class="contact-form">
				<h2 class="row">Get the conversation started</h2>
				<div class="row" style="margin-left: 0;"><?php echo do_shortcode('[contact-form-7 id="1234" title="Contact form 1"]'); ?></div>
			</div>
		</div>
	</div>
	<?php include 'contact.php' ?>
	<?php get_footer(); ?>
</div>

