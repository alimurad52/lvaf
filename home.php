	
<?php /* Template Name: home */  
get_header();?>
<div>
	<div class="jumbotron landing-page" style="background: linear-gradient(to right, rgba(236, 25, 68, 0.7), rgba(132, 45, 106, 1)), url('<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/bg_landingpage.png');background-size: cover">
		<div class="container content-area">
			<h1>Lorem ipsum dolor sit amet dolor</h1>
			<p>We're a premium digital agency that specialises in developing and implementing digital eCommerce and branding strategies.</p>
		</div>
	</div>
	<div class="container-fluid lastest-updates">
		<div class="col-md-4">
			<h1 >Latest updates</h1>
		</div>
		<div class="col-md-4">
			<p >Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
		</div>
		<div class="col-md-4" style="">
			<a href="<?php echo get_home_url(); ?>/?page_id=65"><input class="btn-lva-main" type="button" value="See what's new" ></a>
		</div>
	</div>
	<div class="projects container-fluid">
		<?php 
		$args = array(
			'numberposts' => 2,
			'orderby' => 'post_date',
			'order' => 'ASC',
			'post_type' => 'post'
		);
		$latest_posts = get_posts($args);
		foreach($latest_posts as $latest_post) {
			echo "<div class='container-fluid project'>";
			echo get_the_post_thumbnail($latest_post->ID);
			echo "<div class='after'></div>";
			$category = get_the_category($latest_post->ID);
			$category_parent_id = $category[0]->category_parent;
			$category_parent = get_term($category_parent_id);
			$css_slug = $category_parent->slug." / ".$category[0]->slug;
			echo "<label class='lbl_category'>".$css_slug."</label>";
			echo "<h1 class='post_title'>".$latest_post->post_title."</h1>";
			echo "<a href='".get_permalink($latest_post->ID)."'><input type='button' class='btn-project' value='Discover this project' ></a>";
			echo "</div>";
		}
		?>
	</div>
	<div class="our_work container-fluid">
		<div class="col-md-4">
			<h1>Work</h1>
		</div>
		<div class="col-md-4">
			<p >Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
		</div>
		<div class="col-md-4">
			<input class="btn-lva-main"  type="button" value="See what we've made">
		</div>
	</div>
	<div class="work-wrapper container-fluid">
		<div class="work container-fluid">
			<?php
			$args = array(
				'numberposts' => 8,
				'orderby' => 'post_date',
				'order' => 'ASC',
				'post_type' => 'post'
			);
			$our_projects = get_posts($args);
			$x = 0;
			foreach($our_projects as $our_project) {
				$x++;
				if($x <= 2) {
					continue;
				} else {
					echo "<div class='col-md-4' >";
					echo get_the_post_thumbnail($our_project->ID);
					echo "<div class='after'></div>";
					$category = get_the_category($our_project->ID);
					$category_parent_id = $category[0]->category_parent;
					$category_parent = get_term($category_parent_id);
					$css_slug = $category_parent->slug." / ".$category[0]->slug;
					echo "<label class='lbl_category'>".$css_slug."</label>";
					echo "<h1 class='post_title'>".$our_project->post_title."</h1>";
					echo "<a href='".get_permalink($our_project->ID)."'><input type='button' class='btn-project' value='Discover this project' ></a>";
					echo "</div>";
				}
			}
			?>
		</div>
	</div>
	<div class="digital_strategy">
		<h1 class="box-title">What's your digital strategy?</h1>
		<div class="black_bg">
			<div class="our_approach">
				<img class="bg1" src="<?php echo get_home_url(); ?>/wp-content/themes/lyh-web/src/img/bg1.png">
				<div class="box-our_approach container-fluid">
					<span>Our approach</span>
					<h1>Lorem Ipsum Dolor</h1>
					<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
					<input type="button" class="btn-lva-inverse" value="Discover our passion">
				</div>
			</div>
			<div class="our_services">
				<img class="bg2" src="<?php echo get_home_url(); ?>/wp-content/themes/lyh-web/src/img/bg2.png">
				<div class="box-our_services container-fluid">
					<span>What we offer</span>
					<h1>Lorem Ipsum Dolor</h1>
					<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
					<input type="button" class="btn-lva-inverse" value="Get in touch with us">
				</div>
			</div>
		</div>
	</div>
	<?php include 'contact.php' ?>
	<?php get_footer(); ?>
</div>
