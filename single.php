<?php
/**
 * Single
 *
 * @package thirdbird
 */

get_header(); ?>
<div>
	<section id="single">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php $background_img = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
					<div class="post_bg" style="background-image: url('<?php echo $background_img ?>');">
						<div class="after"></div>
						<div class="page-nav container">
							<div class="row" style="margin-left: 0; margin-right: 0;">
								<a class="col-md-3"><input type="button" class="btn-project" value="Go back" /></a>
								<h1 class="col-md-6 post_title"><?php echo the_title(); ?></h1>
								<a class="col-md-3"><input type="button" class="btn-project" value="Interested? Get in touch with us!" /></a>
							</div>
							<span class="post-quarter"><?php echo get_post_meta(get_the_ID(), "quarterly-meta-box", true);?></span>
						</div>
					</div>
					<div class="company-bg">
						<div class="connector">
							<div class="container-fluid company-stuff">
								<div class="container-fluid company-stuff-bg">
									<div class="container" style="padding: 0;">
										<div class="row">
											<?php 
											$meta_values = get_post_meta( get_the_ID(), 'logo_src', false );	
											foreach ($meta_values as $value){
												$src = $value;
											}
											?>
											<div class="col-md-3 company_logo-wrapper">
												<img class="company_logo" src="<?php echo $src;?>" />
											</div>
											<span class="company_info col-md-6"><?php echo get_post_meta(get_the_ID(), "company_info-meta-box", true);?></span>
											<div class="col-md-3 company_btn-wrapper">
												<a href="<?php echo get_post_meta(get_the_ID(), "company_website-meta-box", true);?>"><input type="button" class="btn-lva-main-inverse" value="<?php echo get_post_meta(get_the_ID(), "company_website-meta-box", true);?>" /></a>
											</div>
										</div>
									</div>
									<div class="company-images">
										<div class="row company-feature_imgs">
											<?php 
											$meta_values = get_post_meta( get_the_ID(), 'feature_img_src', false );
											$count = 0;	
											foreach ($meta_values as $value) {
												if($count == 3) {
													break;
												}
												?>
												<img src = "<?php echo $value?>" class="col-md-4 feature_imgs">
												<?php
												$count++;
											}
											?>

										</div>	
										<img src="<?php echo $meta_values[3]?>" class="last_feature_img" >
									</div>
									<div class="company_content container">
										<div class="project-content"><?php the_content(); ?></div>
									</div>
								</div>
							</div>
							<div class="contact-page"><?php include 'contact.php' ?></div>
						</div>
					</div>
				</article>
			<?php endwhile;  // LOOP END?>
		<?php endif; ?>
	</section>
	<?php get_footer(); ?>
</div>