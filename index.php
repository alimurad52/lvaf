<?php
/**
 * Main
 *
 * @package thirdbird
 */

get_header(); ?>

<main id="primary" class="container-fluid">

	<?php if ( have_posts() ) : ?>
		<div class="col-xs-12 col-sm-12">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header>
					<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>

				</header><!-- .entry-header -->

				<div class="post-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<?php if (get_option('tags_enabled')) : ?>
					<p><?php the_tags(); ?></p>
				<?php endif; ?>

				<?php edit_post_link( __( 'Edit', 'thirdbird' ), '<footer class=""><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

			</article>

			<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>

		<?php endwhile;  // LOOP END?>
		</div>
	<?php endif; ?>

</main>

<?php get_footer(); ?>