<div class="contact-us">
	<div class="jumbotron contact-bg">
		<img src="<?php echo get_home_url(); ?>/wp-content/themes/lyh-web/src/img/contact-bg.png" >
		<div class="after"></div>
	</div>
	<div class="content-contact">
		<h3>Every brand has a story</h3>
		<h1>We can help you tell yours</h1>
		<h2><span><strong>hello</strong>@lyrehouse.de</span></h2>
	</div>
</div>