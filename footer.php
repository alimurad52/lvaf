</div><!-- #content -->
<footer id="colophon" class="container-fluid site-footer" role="contentinfo">
	<div class="row site-info">
		<span class="footer-logo_lyrehouse"><img src="<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/lyre-logo.png" alt="nav-logo" class="navbar-logo-lyrehouse" ><a><i class="far fa-copyright"></i>Lyrehouse UG. All Rights Reserved.</a></span>
		<span class="site-nav">
			<a>Kontakt</a>
			<a>Datenschutz</a>
			<a>Impressum</a>
		</span>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>
</body>
</html>
