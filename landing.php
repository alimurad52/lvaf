<?php /* Template Name: landing */
get_header();?> 
<div class="lyh_landing-page">
	<div class="landing-box">
		<div class="question">
			<p>Have you heard about us?</p>
			<input type="button" class="btn-lva-main submit_yes" value="Yes">
			<input type="button" class="btn-lva-main submit_no" value="No">
		</div>
		<div class="code-box">
			<p>Please enter the code on our business card to enter the site.</p>
			<input type="text" class="input_code" placeholder="Enter code here..">
			<input type="button" class="btn-lva-main" value="Submit">
		</div>
	</div>
	
</div>	
<?php get_footer(); ?>