<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles($hook) {
	$host = $_SERVER['HTTP_HOST'];
	$scriptname = 'scripts.min.js';

	if (preg_match('/localhost/', $host)) {
		$scriptname = 'scripts.js';
	}

	wp_register_script(
		'lyh-web-script',
		get_stylesheet_directory_uri() . '/dist/js/'.$scriptname,
		array('jquery'),
		false,
		true
	);
    wp_enqueue_script('lyh-web-script');

    wp_register_script(
    	'bootstrap',
    	'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
    	array('jquery')
    );
    wp_enqueue_script('bootstrap');
    
}
function quarter_meta_box_markup($object) {
	wp_nonce_field(basename(__FILE__), "quarter_meta-box-nonce");
	?>
	<div>
		<input name="quarterly-meta-box" type="text" value="<?php echo get_post_meta($object->ID, "quarterly-meta-box", true); ?>">
	</div>
	<?php
} 
function company_website_meta_box_markup($object) {
	wp_nonce_field(basename(__FILE__), "company_website_meta-box-nonce");
	?>
	<div>
		<input name="company_website-meta-box" type="text" value="<?php echo get_post_meta($object->ID, "company_website-meta-box", true);?>">
	</div>
	<?php
}
function company_info_meta_box_markup($object) {
	wp_nonce_field(basename(__FILE__), "company_info_meta-box-nonce");
	?>
	<div>
		<textarea style="width: 100%;height: 100px;"  name="company_info-meta-box"><?php echo get_post_meta($object->ID, "company_info-meta-box", true);?></textarea>
	</div>
	<?php
}
function save_company_info_meta_box($post_id, $post, $update) {
	if(!isset($_POST["company_info_meta-box-nonce"]) || !wp_verify_nonce($_POST["company_info_meta-box-nonce"], basename(__FILE__)))
		return $post_id;
	if(!current_user_can("edit_post", $post_id))
		return $post_id;
	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
		return $post_id;
	$slug = "post";
	if($slug != $post->post_type)
		return $post_id;
	$meta_box_text_value = "";
	if(isset($_POST["company_info-meta-box"])) {
		$meta_box_text_value = $_POST["company_info-meta-box"];
	}
	update_post_meta($post_id, "company_info-meta-box", $meta_box_text_value);
}
function save_company_website_meta_box($post_id, $post, $update) {
	if(!isset($_POST["company_website_meta-box-nonce"]) || !wp_verify_nonce($_POST["company_website_meta-box-nonce"], basename(__FILE__)))
		return $post_id;
	if(!current_user_can("edit_post", $post_id))
		return $post_id;
	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
		return $post_id;
	$slug = "post";
	if($slug != $post->post_type)
		return $post_id;
	$meta_box_text_value = "";
	if(isset($_POST["company_website-meta-box"])) {
		$meta_box_text_value = $_POST["company_website-meta-box"];
	}
	update_post_meta($post_id, "company_website-meta-box", $meta_box_text_value);
}
function save_quarterly_meta_box($post_id, $post, $update) {
	if(!isset($_POST["quarter_meta-box-nonce"]) || !wp_verify_nonce($_POST["quarter_meta-box-nonce"], basename(__FILE__)))
		return $post_id;
	if(!current_user_can("edit_post", $post_id))
        return $post_id;
    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
    $slug = "post";
    if($slug != $post->post_type)
        return $post_id;
    $meta_box_text_value = "";
    if(isset($_POST["quarterly-meta-box"]))
    {
        $meta_box_text_value = $_POST["quarterly-meta-box"];
    }   
    update_post_meta($post_id, "quarterly-meta-box", $meta_box_text_value);
}
function image_uploader_metaboxes($object, $box)
{
	wp_nonce_field ( basename ( __FILE__ ), 'image_uploader_metaboxes' );
	global $post;
	$upload_link = esc_url( get_upload_iframe_src() );
	$your_img_id = get_post_meta( get_the_ID(), '_your_img_id', true );
	$your_img_src = wp_get_attachment_image_src( $your_img_id, 'full' );
	$you_have_img = is_array( $your_img_src );
?>
	<div id="custom-images">
		<div class="custom-img-container">
			<?php 
				$meta_values = get_post_meta( get_the_ID(), 'logo_src', false );	
				foreach ($meta_values as $value){
			?>
				<div class="image-wrapper">
					<input type="text" name="logo_src[]" value="<?php echo $value;?>">
					<a class="delete-custom-img" href="#">Remove this image</a>
				</div>
			<?php }?>
		</div>
	</div>
	<p>
	    <a class="upload-custom-img <?php if ( $you_have_img  ) { echo 'hidden'; } ?>" href="<?php echo $upload_link; ?>">
	        <?php _e('Add Company Logo'); ?>
		</a>
	</p>
<?php } 
function save_image_uploader_metadata( $post_id, $post )
{
		if ( !isset( $_POST['image_uploader_metaboxes'] ) || !wp_verify_nonce( $_POST['image_uploader_metaboxes'], basename( __FILE__ ) ) )
			return $post_id;
		$post_type = get_post_type_object( $post->post_type );
		if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
			return $post_id;
		$meta_key = 'logo_src';
		$meta_value = get_post_meta( $post_id, $meta_key, false );
		foreach ($meta_value as $value){
			delete_post_meta( $post_id, $meta_key, $value );
		}
		foreach($_POST['logo_src'] as $value){	
			add_post_meta( $post_id, $meta_key, $value, false );
		}		
}
function enqueue_media(){
	wp_enqueue_media();
}
function include_js_code_for_uploader(){
?>
<script>
	jQuery(function($){
	  var frame,
		  metaBox = $('#image_uploader_metabox.postbox'); // Your meta box id here
		  addImgLink = metaBox.find('.upload-custom-img');
		  imgContainer = metaBox.find( '.custom-img-container');
		  imgIdInput = metaBox.find( '.custom-img-id' );
		  customImgDiv = metaBox.find( '#custom-images' );
	  addImgLink.on( 'click', function( event ){
		event.preventDefault();
		if ( frame ) {
		  frame.open();
		  return;
		}
		frame = wp.media({
		  title: 'Select or Upload Media Of Your Chosen Persuasion',
		  button: {
			text: 'Use this media'
		  },
		  multiple: false  // Set to true to allow multiple files to be selected
		});
 		frame.on( 'select', function() {
		  var attachment = frame.state().get('selection').first().toJSON();
		  imgContainer.append( '<div class="image-wrapper"><input type="text" name="logo_src[]" value="'+attachment.url+'"> <a class="delete-custom-img" href="#">Remove this image</a></div>' );
		  
		});
		frame.open();
	  });
		customImgDiv.on ( 'click', '.delete-custom-img', function (event){		
			event.preventDefault();
			jQuery(event.target).parent().remove();		
		});
	});
</script>
<?php }
function add_image_uploader_metabox()
{	
	add_meta_box(
		'image_uploader_metabox', // Unique ID of metabox
		esc_html__('Company Logo', 'textdomain'), //Title of metabox
		'image_uploader_metaboxes', // Callback function
		'post', //name of your custom post type (here post is for wordpress posts.)
		'normal', //Context
		'default' //Priority
	);
}
function feature_image_upload_metabox($object, $box)
{
	wp_nonce_field ( basename ( __FILE__ ), 'feature_image_upload_metabox' );
	global $post;
	$feature_img_upload_link = esc_url( get_upload_iframe_src() );
	$feature_img_id = get_post_meta( get_the_ID(), 'feature_image', true );
	$feature_img_src = wp_get_attachment_image_src( $feature_img_id, 'full' );
	$got_feature_img = is_array( $feature_img_src );
	?>
	<div id="feature_images">
		<div class="feature-image-container">
			<?php 
			$meta_values = get_post_meta( get_the_ID(), 'feature_img_src', false );	
			foreach ($meta_values as $value){
				?>
				<div class="feature-image-wrapper">
					<input type="text" name="feature_img_src[]" value="<?php echo $value;?>">
					<a class="delete_feature_image" href="#">Remove this image</a>
				</div>
				<?php }?>
			</div>
		</div>
		<p>
			<a class="upload_feature_img <?php if ( $got_feature_img  ) { echo 'hidden'; } ?>" href="<?php echo $feature_img_upload_link; ?>">
				<?php _e('Add Feature Image'); ?>
			</a>
		</p>
		<?php } 
		function save_feature_image_uploader_metadata( $post_id, $post )
		{
			if ( !isset( $_POST['feature_image_upload_metabox'] ) || !wp_verify_nonce( $_POST['feature_image_upload_metabox'], basename( __FILE__ ) ) )
				return $post_id;
			$post_type = get_post_type_object( $post->post_type );
			if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
				return $post_id;
			$meta_key = 'feature_img_src';
			$meta_value = get_post_meta( $post_id, $meta_key, false );
			foreach ($meta_value as $value){
				delete_post_meta( $post_id, $meta_key, $value );
			}
			foreach($_POST['feature_img_src'] as $value){	
				add_post_meta( $post_id, $meta_key, $value, false );
			}		
		}
		function feature_enqueue_media(){
			wp_enqueue_media();
		}
		function include_js_code_for_feature_image_uploader(){
			?>
			<script>
				jQuery(function($){
					var frame,
		  feature_metaBox = $('#feature_image_uploader_metabox.postbox'); // Your meta box id here
		  add_featureimglink = feature_metaBox.find('.upload_feature_img');
		  feature_img_container = feature_metaBox.find( '.feature-image-container');
		  feature_imgidinput = feature_metaBox.find( '.feature-img-id' );
		  featureImgDiv = feature_metaBox.find( '#feature_images' );
		  add_featureimglink.on( 'click', function( event ){
		  	event.preventDefault();
		  	if ( frame ) {
		  		frame.open();
		  		return;
		  	}
		  	frame = wp.media({
		  		title: 'Select or Upload Media Of Your Chosen Persuasion',
		  		button: {
		  			text: 'Use this media'
		  		},
		  multiple: false  // Set to true to allow multiple files to be selected
		});
		  	frame.on( 'select', function() {
		  		var attachment = frame.state().get('selection').first().toJSON();
		  		feature_img_container.append( '<div class="feature-image-wrapper"><input type="text" name="feature_img_src[]" value="'+attachment.url+'"> <a class="delete_feature_image" href="#">Remove this image</a></div>' );
		  		
		  	});
		  	frame.open();
		  });
		  featureImgDiv.on ( 'click', '.delete_feature_image', function (event){		
		  	event.preventDefault();
		  	jQuery(event.target).parent().remove();		
		  });
		});
	</script>
	<?php }
	function add_feature_image_uploader()
	{	
		add_meta_box(
		'feature_image_uploader_metabox', // Unique ID of metabox
		esc_html__('Project Images', 'textdomain'), //Title of metabox
		'feature_image_upload_metabox', // Callback function
		'post', //name of your custom post type (here post is for wordpress posts.)
		'normal', //Context
		'default' //Priority
	);
	}
	add_action ( 'admin_enqueue_scripts', 'feature_enqueue_media' );
	add_action( 'add_meta_boxes', 'add_feature_image_uploader' );
	add_action( 'save_post', 'save_feature_image_uploader_metadata', 10, 2 );
	add_action( 'admin_head', 'include_js_code_for_feature_image_uploader' );
add_action ( 'admin_enqueue_scripts', 'enqueue_media' );
add_action( 'add_meta_boxes', 'add_image_uploader_metabox' );
add_action( 'save_post', 'save_image_uploader_metadata', 10, 2 );
add_action( 'admin_head', 'include_js_code_for_uploader' );
add_action("save_post", "save_company_website_meta_box", 10, 3);
add_action("save_post", "save_quarterly_meta_box", 10, 3);
add_action("save_post", "save_company_info_meta_box", 10, 3);
function add_timeline_meta_box() {
	add_meta_box("quater_time_line", "Project Timeframe", "quarter_meta_box_markup", "post", "side", "default", null);
}
function add_company_website_meta_box() {
	add_meta_box("company_website", "Company Website", "company_website_meta_box_markup", "post", "side", "default", null);
}
function add_company_info_meta_box() {
	add_meta_box("company_info", "Company Info", "company_info_meta_box_markup", "post", "normal", "default", null);
}
add_action("add_meta_boxes", "add_timeline_meta_box");
add_action("add_meta_boxes", "add_company_website_meta_box");
add_action("add_meta_boxes", "add_company_info_meta_box");
?>