function feature_image_upload_metabox($object, $box)
{
	wp_nonce_field ( basename ( __FILE__ ), 'feature_image_upload_metabox' );
	global $post;
	$feature_img_upload_link = esc_url( get_upload_iframe_src() );
	$feature_img_id = get_post_meta( get_the_ID(), 'feature_image', true );
	$feature_img_src = wp_get_attachment_image_src( $feature_img_id, 'full' );
	$got_feature_img = is_array( $feature_img_src );
	?>
	<div id="feature_images">
		<div class="feature-image-container">
			<?php 
			$meta_values = get_post_meta( get_the_ID(), 'feature_img_src', false );	
			foreach ($meta_values as $value){
				?>
				<div class="feature-image-wrapper">
					<input type="text" name="feature_img_src[]" value="<?php echo $value;?>">
					<a class="delete_feature_image" href="#">Remove this image</a>
				</div>
				<?php }?>
			</div>
		</div>
		<p>
			<a class="upload_feature_img <?php if ( $got_feature_img  ) { echo 'hidden'; } ?>" href="<?php echo $feature_img_upload_link; ?>">
				<?php _e('Add Feature Image'); ?>
			</a>
		</p>
		<?php } 
		function save_feature_image_uploader_metadata( $post_id, $post )
		{
			if ( !isset( $_POST['feature_image_upload_metabox'] ) || !wp_verify_nonce( $_POST['feature_image_upload_metabox'], basename( __FILE__ ) ) )
				return $post_id;
			$post_type = get_post_type_object( $post->post_type );
			if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
				return $post_id;
			$meta_key = 'feature_img_src';
			$meta_value = get_post_meta( $post_id, $meta_key, false );
			foreach ($meta_value as $value){
				delete_post_meta( $post_id, $meta_key, $value );
			}
			foreach($_POST['feature_img_src'] as $value){	
				add_post_meta( $post_id, $meta_key, $value, false );
			}		
		}
		function feature_enqueue_media(){
			wp_enqueue_media();
		}
		function include_js_code_for_feature_image_uploader(){
			?>
			<script>
				jQuery(function($){
					var frame,
		  feature_metaBox = $('#feature_image_uploader_metabox.postbox'); // Your meta box id here
		  add_featureimglink = feature_metaBox.find('.upload_feature_img');
		  feature_img_container = feature_metaBox.find( '.feature-image-container');
		  feature_imgidinput = feature_metaBox.find( '.feature-img-id' );
		  featureImgDiv = feature_metaBox.find( '#feature_images' );
		  add_featureimglink.on( 'click', function( event ){
		  	event.preventDefault();
		  	if ( frame ) {
		  		frame.open();
		  		return;
		  	}
		  	frame = wp.media({
		  		title: 'Select or Upload Media Of Your Chosen Persuasion',
		  		button: {
		  			text: 'Use this media'
		  		},
		  multiple: false  // Set to true to allow multiple files to be selected
		});
		  	frame.on( 'select', function() {
		  		var attachment = frame.state().get('selection').first().toJSON();
		  		feature_img_container.append( '<div class="feature-image-wrapper"><input type="text" name="feature_img_src[]" value="'+attachment.url+'"> <a class="delete_feature_image" href="#">Remove this image</a></div>' );
		  		
		  	});
		  	frame.open();
		  });
		  featureImgDiv.on ( 'click', '.delete_feature_image', function (event){		
		  	event.preventDefault();
		  	jQuery(event.target).parent().remove();		
		  });
		});
	</script>
	<?php }
	function add_feature_image_uploader()
	{	
		add_meta_box(
		'feature_image_uploader_metabox', // Unique ID of metabox
		esc_html__('Feature Image', 'textdomain'), //Title of metabox
		'feature_image_upload_metabox', // Callback function
		'post', //name of your custom post type (here post is for wordpress posts.)
		'normal', //Context
		'default' //Priority
	);
	}
	add_action ( 'admin_enqueue_scripts', 'feature_enqueue_media' );
	add_action( 'add_meta_boxes', 'add_feature_image_uploader' );
	add_action( 'save_post', 'save_feature_image_uploader_metadata', 10, 2 );
	add_action( 'admin_head', 'include_js_code_for_feature_image_uploader' );