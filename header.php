<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js" style="margin-top: 0 !important;">
<head>
	<title>
		<?php bloginfo('title'); ?>
	</title>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php //<div id="page-container" class="container-fluid site"> ?>

		<header id="page-header">
			<div class="nav-container"> <!-- part to declare if header is stretched or not -->
				<nav class="navbar navbar-default">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<img src="<?php echo get_site_url(); ?>/wp-content/themes/lyh-web/src/img/lyre-logo.png" alt="nav-logo" class="navbar-logo-lyrehouse" />
							<div class="container hamburger_menu">
								<div class="bar1"></div>
								<div class="bar2"></div>
								<div class="bar3"></div>
							</div>
						</div>
						<div style="position: relative;">
							<div id="myNav" class="overlay"></div>
							<div class="overlay-content">
								<?php
								if ( has_nav_menu('header-menu-right') ) {
									wp_nav_menu( array( 
										'walker' => new WP_Bootstrap_Navwalker(), 
										'theme_location' => 'header-menu-right',
										'container' => 'ul', 
										'menu_id' => 'header-menu-right', 
										'menu_class'     => 'navbar-nav nav pull-right' 
									));  
								}
								?>
							</div>
						</div>
					</div><!-- /.container-fluid -->
				</nav>

			</div>
		</header>

		<?php if ( is_active_sidebar( 'topwidget' ) ) : ?>
			<div class="container">
				<div class="row tb-highlight">
					<?php dynamic_sidebar( 'topwidget' ); ?>
				</div>
			</div>
		<?php endif; ?>


		<div id="site-content" role="content" >


